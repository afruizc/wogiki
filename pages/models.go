package wiki

import (
	"path/filepath"
	"io/ioutil"
	"strings"
)

type Page struct {
	Title string
	Body []byte
}

// Saves the page given a path to save the page to
func (p *Page) save(pathToFile string) error {
	return ioutil.WriteFile(pathToFile, p.Body, 0600)
}

// Loads the page given a load path
func loadPage(pathToFile string) (*Page, error) {
	contents, err := ioutil.ReadFile(pathToFile)
	title := getFileNameNoExtension(pathToFile)
	if err != nil {
		return nil, err
	}

	return &Page{title, contents}, nil
}

func getFileNameNoExtension(pathToFile string) string {
	filename := filepath.Base(pathToFile)
	return strings.TrimSuffix(filename, filepath.Ext(filename))
}

type Config struct {
	// Absolute path for the folder where the templates reside
	templatesPath string

	// Absolute path for the folder where the pages of the wiki reside
	pagesPath string

	// Port the webserver will be listening on
	webserverPort int

	// Extension pages will have when saved to disk
	wikiPageExtension string
}

type Wiki struct {
	config Config
	pages []Page
}

func (w *Wiki) GetAllWikiPages() ([]Page, error) {
	pattern := filepath.Join(w.config.pagesPath, "/*." + w.config.wikiPageExtension)
	pages, err := filepath.Glob(pattern)
	if err != nil {
		return nil, err
	}
}

