package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"html/template"
	"regexp"
)

const (
	pagesLocalPath = "/Users/ruizand/go/src/github.com/afruizc/TripManagement/http_server"
	templatesPath = "/Users/ruizand/go/src/github.com/afruizc/TripManagement/http_server"
)

var (
	templates = template.Must(template.ParseFiles(
		filepath.Join(templatesPath, "edit.html"),
		filepath.Join(templatesPath, "view.html")))
	validPath = regexp.MustCompile("^/(edit|view|save)/([a-zA-Z0-9]+)$")
)


func renderTemplate(w http.ResponseWriter, templateName string, data interface{}) {
	err := templates.ExecuteTemplate(w, templateName + ".html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/" + title, http.StatusFound)
		return
	}

	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}

	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{title, []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/view/" + title, http.StatusFound)
}


func main() {
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))
	fmt.Println("Server listening on 127.0.0.1:5000")
	http.ListenAndServe(":5000", nil)
}
